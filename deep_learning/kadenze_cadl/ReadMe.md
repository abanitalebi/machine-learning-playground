# Deep Learning Course by Kadenze

This package contains code that was used for practicing and 
projects of an online course called "Creative Applications of 
Deep Learning" with TensorFlow by Kadenze.

Course link: https://www.kadenze.com/courses/creative-applications-of-deep-learning-with-tensorflow-iv/sessions

