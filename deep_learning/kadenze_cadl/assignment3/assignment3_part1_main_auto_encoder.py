
import os

import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf

import data
from deep_learning.kadenze_cadl.assignment1.check_libs import ensure_proper_libs_are_available
from deep_learning.kadenze_cadl.assignment1.load_images import load_images
from deep_learning.kadenze_cadl.assignment2.gif import create_gifv_from_list_of_img_arrays
from deep_learning.kadenze_cadl.assignment3.libs import datasets
from deep_learning.kadenze_cadl.assignment3.libs import utils


# Write a function to preprocess/normalize images, given its dataset object (which stores the mean and standard deviation!)
def preprocess(imgs, ds):
    norm_imgs = (imgs - ds.mean()) / ds.std()
    return norm_imgs


# Write a function to undo the normalization of images, given its dataset object (which stores the mean and standard deviation!)
def deprocess(norm_imgs, ds):
    imgs = norm_imgs * ds.std() + ds.mean()
    return imgs


def encode(X, dimensions, activation=tf.nn.tanh):
    Ws = []
    current_input = X
    for layer_i, n_output in enumerate(dimensions):
        scope_name = 'encoder/layer_{}'.format(layer_i)
        with tf.variable_scope(scope_name):
            h, W = utils.linear(current_input, n_output, activation=activation, name=scope_name)
            Ws.append(W)
            current_input = h

    z = current_input
    return Ws, z


def decode(z, dimensions, Ws, activation=tf.nn.tanh):
    current_input = z
    for layer_i, n_output in enumerate(dimensions):
        with tf.variable_scope("decoder/layer_{}".format(layer_i)):
            W = tf.transpose(Ws[layer_i])
            h = tf.matmul(current_input, W)
            current_input = activation(h)
            n_input = n_output
    Y = current_input
    return Y


if __name__ == "__main__":

    """
        - Load and normalize the groceries dataset images as input
        - Auto-encoders
        - Convolutional auto-encoders
        - Denoising auto-encoders 
        - Variational auto-encoders
        - Classification using regression (softmax, cross-entropy etc)
        - Visualizing the training space by exploiting the distribution of inner layer features
        - Saving and loading models
    """

    # Ensure libs are imported properly
    ensure_proper_libs_are_available()

    #############################################################################################
    # Create a dataset object for easy batch operations
    imgs_dir_path = os.path.join(data.__path__[0], 'groceries_dataset', 'freiburg_groceries_dataset', 'images')
    ds = datasets.load_training_part_of_groceries_dataset(imgs_dir_path, flatten=False, split=[1.0, 0.0, 0.0])

    mean_img_float = (ds.mean() / np.amax(ds.mean()))
    plt._imsave('dataset_mean.png', mean_img_float)


    #############################################################################################
    # Build an auto-encoder

    # Number of features in your image = total number of pixels, or (height x width x channels).
    num_images, height, width, num_channels = ds.X.shape
    n_features = height * width * num_channels
    print("Number of features: {}".format(n_features))


    #############################################################################################
    # Build the encoder
    encoder_dimensions = [128, 2]
    X = tf.placeholder(shape=[None, n_features], dtype=tf.float32, name='X')
    assert(X.get_shape().as_list() == [None, n_features])

    Ws, z = encode(X, encoder_dimensions)

    # And just some checks to make sure we've done it right.
    assert(z.get_shape().as_list() == [None, 2])
    assert(len(Ws) == len(encoder_dimensions))

    # If you wanna look at the graph
    # [op.name for op in tf.get_default_graph().get_operations()]
    # [W_i.get_shape().as_list() for W_i in Ws]


    #############################################################################################
    # Build the decoder
    decoder_Ws = Ws[::-1]
    decoder_dimensions = encoder_dimensions[::-1][1:] + [n_features]
    print("decoder_dimensions: {}".format(decoder_dimensions))
    assert(decoder_dimensions[-1] == n_features)

    Y = decode(z, decoder_dimensions, decoder_Ws)

    # [op.name for op in tf.get_default_graph().get_operations() if op.name.startswith('decoder')]
    print("Shape of Y: {}".format(Y.get_shape().as_list()))


    #############################################################################################
    # Set up the loss function
    # Calculate some measure of loss, e.g. the pixel to pixel absolute difference or squared difference
    loss = tf.squared_difference(X, Y)

    # Now sum over every pixel and then calculate the mean over the batch dimension
    cost = tf.reduce_mean(tf.reduce_sum(loss, axis=1))


    #############################################################################################
    # Construct an optimizer
    learning_rate = 0.001
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)


    #############################################################################################
    # Iterate over the data within epochs
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())


    # Some parameters for training
    batch_size = 100
    n_epochs = 3
    step = 5
    examples = ds.X[:batch_size]

    # We have to preprocess the images before feeding them to the network.
    test_examples = preprocess(examples, ds).reshape(-1, n_features)

    # If we want to just visualize them, we can create a montage.
    test_images = utils.montage(examples)

    # Store images so we can make a gif
    gifs = []

    #############################################################################################
    # Now for our training:
    for epoch_i in range(n_epochs):

        # Keep track of the cost
        this_cost = 0

        # Iterate over the entire dataset in batches
        for batch_X, _ in ds.train.next_batch(batch_size=batch_size):

            # Preprocess and reshape our current batch, batch_X:
            this_batch = preprocess(batch_X, ds).reshape(-1, n_features)

            # Compute the cost, and run the optimizer.
            this_cost += sess.run([cost, optimizer], feed_dict={X: this_batch})[0]

        # Average cost of this epoch
        avg_cost = this_cost / ds.X.shape[0] / batch_size
        print("epoch {}, avg cost {}".format(epoch_i, avg_cost))

        # Let's also try to see how the network currently reconstructs the input.
        if epoch_i % step == 0:
            recon = sess.run(Y, feed_dict={X: test_examples})

            # Resize the 2d to the 4d representation:
            rsz = recon.reshape(examples.shape)

            # We have to unprocess the image now, removing the normalization
            unnorm_img = deprocess(rsz, ds)

            # Clip to avoid saturation
            clipped = np.clip(unnorm_img, 0, 255)

            # And we can create a montage of the reconstruction
            recon = utils.montage(clipped)

            # Store for gif
            gifs.append(recon)

            fig, axs = plt.subplots(1, 2, figsize=(10, 10))
            axs[0].imshow(test_images)
            axs[0].set_title('Original')
            axs[1].imshow(recon)
            axs[1].set_title('Synthesis')
            fig.canvas.draw()
            plt.show()

    create_gifv_from_list_of_img_arrays(gifs, output_base_name='final_progress_gif', fps=2)


    #############################################################################################
    # # Visualize the training
    # zs = sess.run(z, feed_dict={X: test_examples})
    # plt.scatter(zs[:, 0], zs[:, 1])
    #
    # # Re-organize to a grid: Assignment problem: https://en.wikipedia.org/wiki/Assignment_problem
    # n_images = 100
    # idxs = np.linspace(np.min(zs) * 2.0, np.max(zs) * 2.0, int(np.ceil(np.sqrt(n_images))))
    # xs, ys = np.meshgrid(idxs, idxs)
    # grid = np.dstack((ys, xs)).reshape(-1, 2)[:n_images,:]
    #
    # fig, axs = plt.subplots(1,2,figsize=(8, 3))
    # axs[0].scatter(zs[:, 0], zs[:, 1], edgecolors='none', marker='o', s=2)
    # axs[0].set_title('Autoencoder Embedding')
    # axs[1].scatter(grid[:,0], grid[:,1], edgecolors='none', marker='o', s=2)
    # axs[1].set_title('Ideal Grid')
    #
    #
    # from scipy.spatial.distance import cdist
    # cost = cdist(grid[:, :], zs[:, :], 'sqeuclidean')
    # from scipy.optimize._hungarian import linear_sum_assignment
    # indexes = linear_sum_assignment(cost)
    #
    # plt.figure(figsize=(5, 5))
    # for i in range(len(zs)):
    #     plt.plot([zs[indexes[1][i], 0], grid[i, 0]],
    #              [zs[indexes[1][i], 1], grid[i, 1]], 'r')
    # plt.xlim([-3, 3])
    # plt.ylim([-3, 3])
    #
    # examples_sorted = []
    # for i in indexes[1]:
    #     examples_sorted.append(examples[i])
    # plt.figure(figsize=(15, 15))
    # img = utils.montage(np.array(examples_sorted))
    # plt.imshow(img, interpolation='nearest')
    # plt.imsave(arr=img, fname='sorted.png')

