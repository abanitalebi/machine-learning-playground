import os

from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf

from deep_learning.kadenze_cadl.assignment3.libs import dataset_utils, utils, dft, datasets


def collect_data_for_one_file(music, frame_hops, n_frames):
    file_i = music[0]
    s = utils.load_audio(file_i)
    plt.plot(s)

    re, im = dft.dft_np(s, hop_size=256, fft_size=512)
    mag, phs = dft.ztoc(re, im)
    print(mag.shape)
    plt.imshow(mag)

    plt.figure(figsize=(10, 4))
    plt.imshow(np.log(mag.T))
    plt.xlabel('Time')
    plt.ylabel('Frequency Bin')

    # We'll therefore have this many sliding windows:
    n_hops = (len(mag) - n_frames) // frame_hops

    Xs = []
    ys = []
    for hop_i in range(n_hops):
        # Creating our sliding window
        frames = mag[(hop_i * frame_hops):(hop_i * frame_hops + n_frames)]

        # Store them with a new 3rd axis and as a logarithmic scale
        # We'll ensure that we aren't taking a log of 0 just by adding
        # a small value, also known as epsilon.
        Xs.append(np.log(np.abs(frames[..., np.newaxis]) + 1e-10))

        # And then store the label
        ys.append(0)

    return Xs, ys


def load_dataset(music, speech, fft_size, hop_size, n_frames, frame_hops):
    # Store every magnitude frame and its label of being music: 0 or speech: 1
    Xs, ys = [], []

    for i in music:
        s = utils.load_audio(i)
        re, im = dft.dft_np(s, fft_size=fft_size, hop_size=hop_size)
        mag, phs = dft.ztoc(re, im)
        n_hops = (len(mag) - n_frames) // frame_hops
        for hop_i in range(n_hops):
            frames = mag[(hop_i * frame_hops):(hop_i * frame_hops + n_frames)]
            this_X = np.log(np.abs(frames[..., np.newaxis]) + 1e-10)
            Xs.append(this_X)
            ys.append(0)

    for i in speech:
        s = utils.load_audio(i)
        re, im = dft.dft_np(s, fft_size=fft_size, hop_size=hop_size)
        mag, phs = dft.ztoc(re, im)
        n_hops = (len(mag) - n_frames) // frame_hops
        for hop_i in range(n_hops):
            frames = mag[(hop_i * frame_hops):(hop_i * frame_hops + n_frames)]
            this_X = np.log(np.abs(frames[..., np.newaxis]) + 1e-10)
            Xs.append(this_X)
            ys.append(1)

    Xs = np.array(Xs)
    ys = np.array(ys)

    return Xs, ys


if __name__ == "__main__":

    """
        - Deep audio classification network: Classify between music and speech
        - The GTZAN Music and Speech dataset has 64 music and 64 speech files, each 30 seconds long, and each at a 
            sample rate of 22050 Hz, meaning there are 22050 samplings of the audio signal per second.
    """

    #############################################################################################
    # Download the dataset
    dst = 'gtzan_music_speech'
    if not os.path.exists(dst):
        dataset_utils.gtzan_music_speech_download(dst)


    #############################################################################################
    # Prepare the dataset
    music_dir = os.path.join(os.path.join(dst, 'music_speech'), 'music_wav')
    music = [os.path.join(music_dir, file_i) for file_i in os.listdir(music_dir) if file_i.endswith('.wav')]
    speech_dir = os.path.join(os.path.join(dst, 'music_speech'), 'speech_wav')
    speech = [os.path.join(speech_dir, file_i) for file_i in os.listdir(speech_dir) if file_i.endswith('.wav')]


    #############################################################################################
    # DFT Analysis
    # Parameters for our dft transform.
    fft_size = 512
    hop_size = 256
    sr = 22050      # The sample rate from our audio is 22050 Hz.

    # We can calculate how many hops there are in a second
    # which will tell us how many frames of magnitudes we have per second
    n_frames_per_second = sr // hop_size

    # We want 500 milliseconds of audio in our window
    n_frames = n_frames_per_second // 2

    # And we'll move our window by 250 ms at a time
    frame_hops = n_frames_per_second // 4


    #############################################################################################
    # Collect data for one file just as an example
    xxs, yyx = collect_data_for_one_file(music, frame_hops, n_frames)


    #############################################################################################
    # Now collect data for all files
    Xs, ys = load_dataset(music, speech, fft_size, hop_size, n_frames, frame_hops)
    print("Xs shape is {}, and ys shape is {}".format(Xs.shape, ys.shape))
    # assert(Xs.shape == (15360, 43, 256, 1) and ys.shape == (15360,))


    #############################################################################################
    # Build the network
    n_observations, n_height, n_width, n_channels = Xs.shape
    ds = datasets.Dataset(Xs=Xs, ys=ys, split=[0.8, 0.1, 0.1], one_hot=True)

    tf.reset_default_graph()
    X = tf.placeholder(name='X', shape=[None, n_height, n_width, n_channels], dtype=tf.float32)
    Y = tf.placeholder(name='Y', shape=[None, 2], dtype=tf.float32)

    n_filters = [9, 9, 9, 9]

    H = X
    for layer_i, n_filters_i in enumerate(n_filters):
        H, W = utils.conv2d(H, n_filters_i, k_h=3, k_w=3, d_h=2, d_w=2, name=str(layer_i))
        H = tf.nn.relu(H)

    fc, W = utils.linear(H, n_output=50, name="linear", activation=tf.nn.relu, reuse=None)
    Y_pred, W = utils.linear(fc, n_output=2, name="softmax", activation=tf.nn.softmax, reuse=None)

    loss = utils.binary_cross_entropy(Y_pred, Y)
    cost = tf.reduce_mean(tf.reduce_sum(loss, 1))

    predicted_y = tf.argmax(Y_pred, 1)
    actual_y = tf.argmax(Y, 1)
    correct_prediction = tf.equal(predicted_y, actual_y)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    learning_rate = 0.01
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

    n_epochs = 10
    batch_size = 200

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    # Now iterate over our dataset n_epoch times
    for epoch_i in range(n_epochs):
        print('Epoch: ', epoch_i)

        # Training
        this_accuracy = 0
        its = 0
        for Xs_i, ys_i in ds.train.next_batch(batch_size):
            this_accuracy += sess.run([accuracy, optimizer], feed_dict={X:Xs_i, Y:ys_i})[0]
            its += 1
        print('    Training accuracy: ', this_accuracy / its)

        # Validation (see how the network does on unseen data).
        this_accuracy = 0
        its = 0
        for Xs_i, ys_i in ds.valid.next_batch(batch_size):
            this_accuracy += sess.run(accuracy, feed_dict={X:Xs_i, Y:ys_i})
            its += 1
        print('    Validation accuracy: ', this_accuracy / its)

    # Testing
    this_accuracy = 0
    its = 0
    for Xs_i, ys_i in ds.test.next_batch(batch_size):
        this_accuracy += sess.run(accuracy, feed_dict={X:Xs_i, Y:ys_i})
        its += 1
    print('Test accuracy: ',this_accuracy / its)

    # g = tf.get_default_graph()
    # [op.name for op in g.get_operations()]

    g = tf.get_default_graph()
    for layer_i in range(len(n_filters)):
        W = sess.run(g.get_tensor_by_name('{}/W:0'.format(layer_i)))
        plt.figure(figsize=(5, 5))
        plt.imshow(utils.montage_filters(W))
        plt.title('Layer {}\'s Learned Convolution Kernels'.format(layer_i))
        plt.savefig("audio_classifier_network_layer_{}.png".format(layer_i))
        plt.close()


