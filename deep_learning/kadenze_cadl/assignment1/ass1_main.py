import os

import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf

import data
from deep_learning.kadenze_cadl.assignment1.check_libs import ensure_proper_libs_are_available
from deep_learning.kadenze_cadl.assignment1.load_images import load_images
from deep_learning.kadenze_cadl.assignment1.libs import utils

DATA_LOCATION = 'data/sample_100_images'

if __name__ == "__main__":

    """
        - Load 100 images
        - Resize, and normalize them to a shape of 100 X 100 X 100 X 3
        - Convolve with a Gabor kernel to get 100 X 100 X 100 edge images
    """


    # Step 1: Ensure libs are imported properly
    ensure_proper_libs_are_available()


    # Step 2: Read the images and create a montage snapshot of all
    dirpath = os.path.join(data.__path__[0], 'sample_100_images')
    imgs = load_images(dirpath)


    # Step 3: Compute mean and Standard Deviation
    # First create a tensorflow session
    sess = tf.InteractiveSession()

    # Now create an operation that will calculate the mean of your images
    mean_img_op = tf.reduce_mean(imgs, axis=0)

    # And then run that operation using your session
    mean_img = sess.run(mean_img_op)

    # Then plot the resulting mean image:
    plt.figure(figsize=(10, 10))
    plt.imshow(mean_img)
    plt.imsave(arr=mean_img, fname='mean.png')

    mean_img_4d = tf.reduce_mean(imgs, axis=0, keep_dims=True)
    subtraction = imgs - mean_img_4d
    std_img_op = tf.sqrt(tf.reduce_mean(subtraction * subtraction, axis=0))
    std_img = sess.run(std_img_op)

    plt.figure(figsize=(10, 10))
    std_img_show = std_img / np.max(std_img)
    plt.imshow(std_img_show)
    plt.imsave(arr=std_img_show, fname='std.png')


    # Step 4: Normalize the dataset
    std_img_4d = tf.sqrt(tf.reduce_mean(subtraction * subtraction, axis=0, keep_dims=True))
    norm_imgs_op = (imgs - mean_img_4d) / std_img_4d
    norm_imgs = sess.run(norm_imgs_op)
    print('min and max of the images:', np.min(norm_imgs), np.max(norm_imgs))
    print('image value data types', imgs.dtype)

    plt.figure(figsize=(10, 10))
    plt.imshow(utils.montage(norm_imgs, 'normalized.png'))

    norm_imgs_show = (norm_imgs - np.min(norm_imgs)) / (np.max(norm_imgs) - np.min(norm_imgs))
    plt.figure(figsize=(10, 10))
    plt.imshow(utils.montage(norm_imgs_show, 'normalized.png'))


    # Step 5: Convolve the dataset using a Gabor edge detection kernel filter
    # First build 3 kernels for each input color channel
    ksize = 16
    kernel = np.concatenate([utils.gabor(ksize)[:, :, np.newaxis] for i in range(3)], axis=2)

    # Now make the kernels into the shape: [ksize, ksize, 3, 1]:
    kernel_4d = np.reshape(kernel, (ksize, ksize, 3, 1))
    assert(kernel_4d.shape == (ksize, ksize, 3, 1))

    plt.figure(figsize=(5, 5))
    plt.imshow(kernel_4d[:, :, 0, 0], cmap='gray')
    plt.imsave(arr=kernel_4d[:, :, 0, 0], fname='kernel.png', cmap='gray')

    convolved = utils.convolve(imgs, kernel_4d)
    convolved_show = (convolved - np.min(convolved)) / (np.max(convolved) - np.min(convolved))
    print(convolved_show.shape)
    plt.figure(figsize=(10, 10))
    plt.imshow(utils.montage(convolved_show[:, :, :, 0], 'convolved.png'), cmap='gray')


    # Step 6: Sort the dataset of images based on their mean value
    # Create a set of operations using tensorflow which could provide you for instance the sum or mean value of every
    # image in your dataset:

    # First flatten our convolved images so instead of many 3d images,
    # we have many 1d vectors.
    # This should convert our 4d representation of N x H x W x C to a
    # 2d representation of N x (H*W*C)
    N, H, W, C = convolved.shape
    flattened = tf.reshape(convolved, (N, H*W*C))
    assert(flattened.get_shape().as_list() == [100, 10000])

    # Now calculate some statistics about each of our images
    values = tf.reduce_sum(flattened, axis=1)

    # Then create another operation which sorts those values
    # and then calculate the result:
    idxs_op = tf.nn.top_k(values, k=100)[1]
    idxs = sess.run(idxs_op)

    # Then finally use the sorted indices to sort your images:
    sorted_imgs = np.array([imgs[idx_i] for idx_i in idxs])

    # Then plot the resulting sorted dataset montage:
    # Make sure we have a 100 x 100 x 100 x 3 dimension array
    assert(sorted_imgs.shape == (100, 100, 100, 3))
    plt.figure(figsize=(10, 10))
    plt.imshow(utils.montage(sorted_imgs, 'sorted.png'))

