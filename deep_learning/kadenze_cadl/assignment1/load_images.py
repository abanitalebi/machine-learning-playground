import os

from matplotlib import pyplot as plt
import numpy as np
from skimage.transform import resize

from deep_learning.kadenze_cadl.assignment1.libs import utils


def load_images(dirpath, show_plot=True):
    # Load every image file in the provided directory
    filepaths = [os.path.join(dirpath, fname) for fname in os.listdir(dirpath)]

    # Read every filename as an RGB image
    imgs = [plt.imread(fname)[..., :3] for fname in filepaths]

    # Crop every image to a square
    imgs = [utils.imcrop_tosquare(img_i) for img_i in imgs]

    # Then resize the square image to 100 x 100 pixels
    imgs = [resize(img_i, (100, 100)) for img_i in imgs]

    # Finally make our list of 3-D images a 4-D array with the first dimension the number of images:
    imgs = np.array(imgs).astype(np.float32)

    # Plot the resulting dataset:
    # Make sure you "run" this cell after you create your `imgs` variable as a 4-D array!
    # Make sure we have a 100 x 100 x 100 x 3 dimension array
    assert(imgs.shape == (100, 100, 100, 3))
    if show_plot:
        plt.figure(figsize=(10, 10))
        plt.imshow(utils.montage(imgs, saveto='dataset.png'))

    return imgs