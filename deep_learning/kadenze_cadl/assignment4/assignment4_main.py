
import os

import numpy as np
from matplotlib import pyplot as plt
import tensorflow as tf

import data
from deep_learning.kadenze_cadl.assignment1.check_libs import ensure_proper_libs_are_available
from deep_learning.kadenze_cadl.assignment1.load_images import load_images
from deep_learning.kadenze_cadl.assignment3.libs import datasets
from deep_learning.kadenze_cadl.assignment3.libs import utils


if __name__ == "__main__":

    """
        - find the gradient of a neuron (or max of neurons etc - eventually one single number), with respect to a noise
                image. That gives patterns that that neuron understands about the input image.
        - Get a tensor, output of a network layer, then perform tf.gradients on that layer.
        - This generates 2D visualizations. In each iteration, add the current gradients to the previous noise image, so
                the pattern is understood over time.
        - This is called deep dream. To improve the visualization: add decay (multiply say by 0.95), blur the noise 
                image every 5 iterations, clipping 1% of lower values, zooming (crop a pixel from the sides, the 
                re-scale with imresize)
        - StyleNet: to get content from one image, and style from another image.
        - Feed contents (e.g. an image) as input to a network in the form of tf.variable(img4d) so that this picture is 
                optimized to preserve a style. You can also start with a noise image
    """

