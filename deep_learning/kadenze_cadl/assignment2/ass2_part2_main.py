
from matplotlib import pyplot as plt
import numpy as np
from scipy.misc import imresize
import tensorflow as tf

from deep_learning.kadenze_cadl.assignment1.check_libs import ensure_proper_libs_are_available
from deep_learning.kadenze_cadl.assignment2 import gif
from deep_learning.kadenze_cadl.assignment2.utils import linear, split_image, calc_distance, train

if __name__ == "__main__":

    """
        - Load and normalize an image
        - Learn how to re-paint that image using an 8 layer fully connected network
    """

    # Ensure libs are imported properly
    ensure_proper_libs_are_available()

    #############################################################################################
    # Step 1: Load the input data (image coordinates) and output data (RGB values)
    # First load an image
    img = plt.imread('part2_in.jpg')

    # Be careful with the size of your image. Try a fairly small image to begin with, then come back here and try larger sizes.
    img = imresize(img, (100, 100))
    plt.figure(figsize=(5, 5))
    plt.imshow(img)

    # Make sure you save this image as "reference.png" and include it in your zipped submission file
    # so we can tell what image you are trying to paint!
    plt.imsave(fname='part2_reference.png', arr=img)

    xs, ys = split_image(img)

    # and print the shapes
    print('X_shape: {}, Y_shape: {}'.format(xs.shape, ys.shape))

    # Normalize the input (xs) using its mean and standard deviation
    xs = (xs - np.mean(xs)) / (np.std(xs))

    # Just to make sure you have normalized it correctly:
    print(np.min(xs), np.max(xs))
    assert(np.min(xs) > -3.0 and np.max(xs) < 3.0)

    print(np.min(ys), np.max(ys))
    ys = ys / 255.0
    print(np.min(ys), np.max(ys))
    plt.imshow(ys.reshape(img.shape))


    #############################################################################################
    # Step 2: Construct a network, to learn that image
    # Let's reset the graph:
    tf.reset_default_graph()

    # Create a placeholder of None x 2 dimensions and dtype tf.float32
    # This will be the input to the network which takes the row/col
    X = tf.placeholder(shape=[None, 2], dtype=tf.float32, name='X')

    # Create the placeholder, Y, with 3 output dimensions instead of 2.
    # This will be the output of the network, the R, G, B values.
    Y = tf.placeholder(shape=[None, 3], dtype=tf.float32, name='Y')

    n_neurons = [2, 20, 20, 20, 20, 20, 20, 3]

    current_input = X
    for layer_i in range(1, len(n_neurons)):
        current_input, current_W = linear(
            X=current_input,
            n_output=n_neurons[layer_i],
            activation=tf.nn.relu if (layer_i+1) < len(n_neurons) else None,
            name='layer_' + str(layer_i))
    Y_pred = current_input

    cost = tf.reduce_mean(tf.reduce_sum(calc_distance(Y_pred, Y), 1))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001).minimize(cost)

    n_iterations = 500
    batch_size = 50
    imgs = []
    costs = []
    with tf.Session() as sess:
        # Here we tell tensorflow that we want to initialize all the variables in the graph so we can use them
        # This will set W and b to their initial random normal value.
        sess.run(tf.global_variables_initializer())

        # We now run a loop over epochs
        prev_training_cost = 0.0
        for it_i in range(n_iterations):
            idxs = np.random.permutation(range(len(xs)))
            n_batches = len(idxs) // batch_size
            for batch_i in range(n_batches):
                idxs_i = idxs[batch_i * batch_size: (batch_i + 1) * batch_size]
                sess.run(optimizer, feed_dict={X: xs[idxs_i], Y: ys[idxs_i]})

            training_cost = sess.run(cost, feed_dict={X: xs, Y: ys})
            print(it_i, training_cost)

            if (it_i + 1) % 20 == 0:
                costs.append(training_cost)
                ys_pred = 255 * Y_pred.eval(feed_dict={X: xs}, session=sess)
                img = np.clip(ys_pred.reshape(img.shape), 0, 255).astype(np.uint8)
                imgs.append(img)
                # Plot the cost over time
                fig, ax = plt.subplots(1, 2)
                ax[0].plot(costs)
                ax[0].set_xlabel('Iteration')
                ax[0].set_ylabel('Cost')
                ax[1].imshow(img)
                fig.suptitle('Iteration {}'.format(it_i))
                plt.savefig('part2_reconstructed.png')
                plt.close()

        # Save the images as a GIF
        _ = gif.build_gif(imgs, saveto='part2_animation.mp4', show_gif=False)

