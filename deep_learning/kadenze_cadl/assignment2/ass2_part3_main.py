
import os

import numpy as np
import tensorflow as tf

import data
from deep_learning.kadenze_cadl.assignment1.check_libs import ensure_proper_libs_are_available
from deep_learning.kadenze_cadl.assignment1.load_images import load_images
from deep_learning.kadenze_cadl.assignment2.gif import create_gifv_from_list_of_img_arrays
from deep_learning.kadenze_cadl.assignment2.utils import train, montage

if __name__ == "__main__":

    """
        - Load and normalize 100 images as input
        - Build an 8 layer fully connected network to learn how to re-paint those images
    """

    # Ensure libs are imported properly
    ensure_proper_libs_are_available()

    #############################################################################################
    # Step 1: Load the input data - 100 images
    dirpath = os.path.join(data.__path__[0], 'sample_100_images')
    imgs = load_images(dirpath)


    #############################################################################################
    # Step 3: Learning more than one image
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    gifs = train(imgs=imgs,
                 learning_rate=0.001,
                 batch_size=200,
                 n_iterations=200,
                 gif_step=2,
                 n_neurons=30,
                 n_layers=10,
                 activation_fn=tf.nn.relu,
                 final_activation_fn=tf.nn.tanh,
                 cost_type='l2_norm')

    montage_gifs = [montage(255*(m - np.amin(m))/(np.amax(m) - np.amin(m))).astype(np.uint8) for m in gifs]
    create_gifv_from_list_of_img_arrays(montage_gifs, output_base_name='montage_gif', fps=1)

    final = gifs[-1]
    final_gif = [(255*(m - np.amin(m))/(np.amax(m) - np.amin(m))).astype(np.uint8) for m in final]
    create_gifv_from_list_of_img_arrays(final_gif, output_base_name='final_gif', fps=2)
