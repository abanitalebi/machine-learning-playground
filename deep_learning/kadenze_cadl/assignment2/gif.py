import glob

import imageio as imageio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def build_gif(imgs, interval=0.1, dpi=72,
              save_gif=True, saveto='animation.mp4',
              show_gif=False, cmap=None):
    """Take an array or list of images and create a GIF.

    Parameters
    ----------
    imgs : np.ndarray or list
        List of images to create a GIF of
    interval : float, optional
        Spacing in seconds between successive images.
    dpi : int, optional
        Dots per inch.
    save_gif : bool, optional
        Whether or not to save the GIF.
    saveto : str, optional
        Filename of GIF to save.
    show_gif : bool, optional
        Whether or not to render the GIF using plt.
    cmap : None, optional
        Optional colormap to apply to the images.

    Returns
    -------
    ani : matplotlib.animation.ArtistAnimation
        The artist animation from matplotlib.  Likely not useful.
    """
    imgs = np.asarray(imgs)
    h, w, *c = imgs[0].shape
    fig, ax = plt.subplots(figsize=(np.round(w / dpi), np.round(h / dpi)))
    fig.subplots_adjust(bottom=0)
    fig.subplots_adjust(top=1)
    fig.subplots_adjust(right=1)
    fig.subplots_adjust(left=0)
    ax.set_axis_off()

    if cmap is not None:
        axs = list(map(lambda x: [
            ax.imshow(x, cmap=cmap)], imgs))
    else:
        axs = list(map(lambda x: [
            ax.imshow(x)], imgs))

    ani = animation.ArtistAnimation(fig, axs, interval=interval*1000, repeat_delay=0, blit=False)

    if save_gif:
        try:
            # ani.save(saveto, writer='imagemagick', dpi=dpi)
            FFwriter = animation.FFMpegWriter(fps=1, extra_args=['-vcodec', 'libx264'])
            ani.save(saveto, writer=FFwriter, dpi=dpi)
        except FileNotFoundError:
            print('You do not have ffmpeg installed.')
        # except:
        #     print('You do not have imagemagick installed.\n\nOn OSX ' +
        #           'you can install this by first installing homebrew: ' +
        #           'http://brew.sh\nThen run: "brew install imagemagick".\n' +
        #           'Windows users can obtain a binary installation here: ' +
        #           'https://www.imagemagick.org/script/binary-releases.php\n' +
        #           'And Linux users should be able to install imagemagick using ' +
        #           'their package manager, e.g.: sudo apt-get install imagemagick.')

    if show_gif:
        plt.show()

    return ani


def create_gifv_from_img_files(input_glob, output_base_name, fps):
    output_extensions = ["gif", "mp4"]
    input_filenames = glob.glob(input_glob)

    poster_writer = imageio.get_writer("{}.png".format(output_base_name), mode='i')
    video_writers = [imageio.get_writer("{}.{}".format(output_base_name, ext), mode='I', fps=fps) for ext in output_extensions]

    is_first = True
    for filename in input_filenames:
        img = imageio.imread(filename)

        # ... processing to crop, rescale, etc. goes here

        for writer in video_writers:
            writer.append_data(img)
        if is_first:
            poster_writer.append_data(img)

        is_first = False

    for writer in video_writers + [poster_writer]:
        writer.close()


def create_gifv_from_list_of_img_arrays(imgs, output_base_name, fps):
    output_extensions = ["gif", "mp4"]

    poster_writer = imageio.get_writer("{}.png".format(output_base_name), mode='i')
    video_writers = [imageio.get_writer("{}.{}".format(output_base_name, ext), mode='I', fps=fps) for ext in output_extensions]

    is_first = True
    for img in imgs:
        # ... processing to crop, rescale, etc. goes here

        for writer in video_writers:
            writer.append_data(img)
        if is_first:
            poster_writer.append_data(img)

        is_first = False

    for writer in video_writers + [poster_writer]:
        writer.close()
