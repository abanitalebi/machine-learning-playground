import numpy as np
import tensorflow as tf

from deep_learning.kadenze_cadl.assignment5.libs import celeb_vaegan
from deep_learning.kadenze_cadl.assignment5.libs import utils, vaegan
from deep_learning.kadenze_cadl.assignment5.libs.vaegan import test_celeb


def test_vaegan_training():
    utils.get_celeb_files()
    vaegan.test_celeb(n_epochs=1,
                      crop_shape=[32, 32, 3],
                      n_filters=[10],
                      filter_sizes=[3])


def test_celeb_vaegan():
    g = tf.Graph()
    with tf.Session(graph=g) as sess:
        net = celeb_vaegan.get_celeb_vaegan_model()
        tf.import_graph_def(
            net['graph_def'],
            name='net',
            input_map={
                'encoder/variational/random_normal:0':
                    np.zeros(512, dtype=np.float32)
            }
        )
        names = [op.name for op in g.get_operations()]
        print(names)


if __name__ == "__main__":

    """
        - Variational GANs (easier than GAN to train)        
    """

    #############################################################################################
    # The real code, to train the variational GAN
    test_celeb()

    # To test the trained network
    # test_vaegan_training()
    # test_celeb_vaegan()


