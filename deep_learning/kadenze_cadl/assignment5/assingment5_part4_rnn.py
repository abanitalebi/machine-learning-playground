import tensorflow as tf

from deep_learning.kadenze_cadl.assignment5.libs import charrnn


def test_alice():
    charrnn.test_alice()


def test_trump():
    charrnn.test_trump()


if __name__ == "__main__":

    """
        - Useful blogs on RNN/LSTM: 
            http://karpathy.github.io/2015/05/21/rnn-effectiveness/
            http://colah.github.io/posts/2015-08-Understanding-LSTMs/
        - Part4: RNN/LSTM
        
    """

    #############################################################################################
    # Character level language modeling with LSTM - predicting next characters
    test_alice()
