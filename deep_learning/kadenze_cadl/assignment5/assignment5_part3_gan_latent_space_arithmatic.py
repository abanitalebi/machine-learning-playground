from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf

from deep_learning.kadenze_cadl.assignment5.libs import celeb_vaegan
from deep_learning.kadenze_cadl.assignment5.libs import datasets


def slerp(val, low, high):
    """Spherical interpolation. val has a range of 0 to 1."""
    if val <= 0:
        return low
    elif val >= 1:
        return high
    omega = np.arccos(np.dot(low/np.linalg.norm(low), high/np.linalg.norm(high)))
    so = np.sin(omega)
    return np.sin((1.0-val)*omega) / so * low + np.sin(val*omega)/so * high


def get_features_for(files, label='Bald', has_label=True, n_imgs=50):
    label_i = net['labels'].index(label)
    label_idxs = np.where(net['attributes'][:, label_i] == has_label)[0]
    label_idxs = np.random.permutation(label_idxs)[:n_imgs]
    imgs = [plt.imread(files[img_i])[..., :3]
            for img_i in label_idxs]
    preprocessed = np.array([celeb_vaegan.preprocess(img_i) for img_i in imgs])
    zs = sess.run(Z, feed_dict={X: preprocessed})
    return np.mean(zs, 0)


if __name__ == "__main__":

    """
        - Latent space arithmetics in GANs        
    """

    #############################################################################################
    # Load Celebrity face dataset of images and use a pre-trained GAN to perform latent space arithmetics
    tf.reset_default_graph()
    net = celeb_vaegan.get_celeb_vaegan_model()

    sess = tf.Session()
    g = tf.get_default_graph()
    tf.import_graph_def(net['graph_def'], name='net', input_map={
        'encoder/variational/random_normal:0': np.zeros(512, dtype=np.float32)})
    names = [op.name for op in g.get_operations()]
    print("operation names in the pre-trained network: {}".format(names))

    X = g.get_tensor_by_name('net/x:0')    # Discriminator's input
    Z = g.get_tensor_by_name('net/encoder/variational/z:0')    # Generator's input = latent space features
    G = g.get_tensor_by_name('net/generator/x_tilde:0')    # Generator's output

    files = datasets.CELEB()


    # Explore different attributes
    z1 = get_features_for(files, 'Male', True, n_imgs=10)
    z2 = get_features_for(files, 'Male', False, n_imgs=10)
    z3 = get_features_for(files, 'Smiling', True, n_imgs=10)
    z4 = get_features_for(files, 'Smiling', False, n_imgs=10)

    b1 = sess.run(G, feed_dict={Z: z1[np.newaxis]})
    b2 = sess.run(G, feed_dict={Z: z2[np.newaxis]})
    b3 = sess.run(G, feed_dict={Z: z3[np.newaxis]})
    b4 = sess.run(G, feed_dict={Z: z4[np.newaxis]})

    fig, axs = plt.subplots(1, 4, figsize=(15, 6))
    axs[0].imshow(b1[0] / b1.max()), axs[0].set_title('Male'), axs[0].grid('off'), axs[0].axis('off')
    axs[1].imshow(b2[0] / b2.max()), axs[1].set_title('Not Male'), axs[1].grid('off'), axs[1].axis('off')
    axs[2].imshow(b3[0] / b3.max()), axs[2].set_title('Smiling'), axs[2].grid('off'), axs[2].axis('off')
    axs[3].imshow(b4[0] / b4.max()), axs[3].set_title('Not Smiling'), axs[3].grid('off'), axs[3].axis('off')
    plt.savefig('gan_latent_space_arithmetics_1.png')


    # Male to female transition
    notmale_vector = z2 - z1
    n_imgs = 5
    amt = np.linspace(0, 1, n_imgs)
    zs = np.array([z1 + notmale_vector*amt_i for amt_i in amt])
    g = sess.run(G, feed_dict={Z: zs})

    fig, axs = plt.subplots(1, n_imgs, figsize=(20, 4))
    for i, ax_i in enumerate(axs):
        ax_i.imshow(np.clip(g[i], 0, 1))
        ax_i.grid('off')
        ax_i.axis('off')
    plt.savefig('gan_latent_space_arithmetics_male_to_female_linear_interp.png')


    # Neutral to smiling transition
    smiling_vector = z3 - z4
    amt = np.linspace(0, 1, n_imgs)
    zs = np.array([z4 + smiling_vector*amt_i for amt_i in amt])
    g = sess.run(G, feed_dict={Z: zs})
    fig, axs = plt.subplots(1, n_imgs, figsize=(20, 4))
    for i, ax_i in enumerate(axs):
        ax_i.imshow(np.clip(g[i] / g[i].max(), 0, 1))
        ax_i.grid('off')
    plt.savefig('gan_latent_space_arithmetics_neutral_to_smiling_linear_interp.png')


    # Neutral to smiling transition - using SLERP interpolation
    amt = np.linspace(0, 1, n_imgs)
    zs = np.array([slerp(amt_i, z1, z2) for amt_i in amt])
    g = sess.run(G, feed_dict={Z: zs})
    fig, axs = plt.subplots(1, n_imgs, figsize=(20, 4))
    for i, ax_i in enumerate(axs):
        ax_i.imshow(np.clip(g[i], 0, 1))
        ax_i.grid('off')
        ax_i.axis('off')
    plt.savefig('gan_latent_space_arithmetics_male_to_female_slerp.png')


    # Now I'll try it on my own picture!
    file_path = 'Amin.jpg'
    img = plt.imread(file_path)[..., :3]
    img = celeb_vaegan.preprocess(img, crop_factor=1.0)[np.newaxis]
    img_features = sess.run(Z, feed_dict={X: img})

    amt = np.linspace(0, 1, n_imgs)
    zs = np.array([np.squeeze(img_features) + notmale_vector*amt_i for amt_i in amt])
    g = sess.run(G, feed_dict={Z: zs})
    fig, axs = plt.subplots(1, n_imgs, figsize=(20, 4))
    for i, ax_i in enumerate(axs):
        ax_i.imshow(np.clip(g[i] / g[i].max(), 0, 1))
        ax_i.grid('off')
    plt.savefig('gan_latent_space_arithmetics_male_to_female_linear_interp_Amin.png')
