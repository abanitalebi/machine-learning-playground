import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D


def plot_act(i=1.0, actfunc=lambda x: x):
    ws = np.arange(-0.5, 0.5, 0.05)
    bs = np.arange(-0.5, 0.5, 0.05)

    X, Y = np.meshgrid(ws, bs)

    os = np.array([actfunc(tf.constant(w*i + b)).eval(session=sess) \
                   for w,b in zip(np.ravel(X), np.ravel(Y))])

    Z = os.reshape(X.shape)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, Z, rstride=1, cstride=1)


if __name__ == "__main__":

    sess = tf.Session()
    i = tf.constant([1.0, 2.0, 3.0], shape=[1, 3])
    w = tf.random_normal(shape=[3, 3])
    b = tf.random_normal(shape=[1, 3])

    #dummy activation function
    def func(x): return x
    act = func(tf.matmul(i, w) + b)
    act.eval(session=sess)

    # 1) Step function
    plot_act(1.0, func)

    # 2) Sigmoid function
    plot_act(1, tf.sigmoid)
    act = tf.sigmoid(tf.matmul(i, w) + b)
    act.eval(session=sess)

    # 3) Hyperbolic tangent
    plot_act(1, tf.tanh)
    act = tf.tanh(tf.matmul(i, w) + b)
    act.eval(session=sess)

    # 4) ReLU: this kind of activation function is directly relatable to the nervous system analogy of Neural Networks
    # (this is called Biological Plausibility)
    plot_act(1, tf.nn.relu)
    act = tf.nn.relu(tf.matmul(i, w) + b)
    act.eval(session=sess)
